import re
import json
import scrapy
import websockets


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['fornida.com']
    start_urls = ["data:,"]

    uri = 'wss://s-usc1c-nss-294.firebaseio.com/.ws?v=5&ns=fornida-6f0cf'
    all_products_msg = '{"t":"d","d":{"r":3,"a":"q","b":{"p":"/all_products","h":""}}}'
    configuration_msg = '{"t":"d","d":{"r":24,"a":"q","b":{"p":"/products/%s/data","h":""}}}'


    async def parse(self, response):
        """
            Get all products via websocket
        """

        # ping_interval=30, ping_timeout=30, close_timeout=30
        # https://websockets.readthedocs.io/en/stable/api.html#websockets.protocol.WebSocketCommonProtocol
        async with websockets.connect(self.uri) as ws:
            await ws.send(self.all_products_msg)

            messages = []

            # async for msg in ws:
            #     if msg.tp == aiohttp.MsgType.text:
            #         if type(msg.data) == str and ('{' in msg.data or '}' in msg.data):
            #             messages.append(msg.data)
            #     elif msg.tp == aiohttp.MsgType.closed:
            #         break
            #     elif msg.tp == aiohttp.MsgType.error:
            #         break

            try:
                async for msg in ws:
                    if type(msg) == str and ('{' in msg or '}' in msg):
                        messages.append(msg)
            except websockets.ConnectionClosed:
                pass

        data = self.get_msg(messages, 'all_products')

        try:
            all_products = json.loads(data['d']['b']['d'])
        except Exception as e:
            self.logger.error('Unable to parse json', exc_info=True)
            return

        for product in all_products:
            url = 'https://fornida.com/api/getProductById?product_id={}'.format(product['id'])
            yield scrapy.Request(url, callback=self.parse_product)


    async def parse_product(self, response):
        try:
            product = json.loads(response.text)
            product = product['data']
        except Exception as e:
            self.logger.warning('Unable to parse json', exc_info=True)
            self.logger.debug(data)
            return

        if 'table-specs' in product['description']:
            product['specs'] = self.parse_specs(response.replace(body=product['description']))
        else:
            product['specs'] = {}

        sku = re.sub('[^a-z0-9]', '', product['sku'].lower())

        # ping_interval=30, ping_timeout=30, close_timeout=30
        # https://websockets.readthedocs.io/en/stable/api.html#websockets.protocol.WebSocketCommonProtocol
        async with websockets.connect(self.uri, close_timeout=10) as ws:
            await ws.send(self.configuration_msg % sku)

            messages = []

            try:
                async for msg in ws:
                    if type(msg) == str and ('{' in msg or '}' in msg):
                        messages.append(msg)
            except websockets.ConnectionClosed:
                pass

        data = self.get_msg(messages, '/products/')

        if data:
            product['config'] = data['d']['b']['d']['config']
        else:
            url = 'https://fornida.com/product' + product.get('custom_url', {}).get('url', '/NA/') + str(product['id'])
            self.logger.info('Unable to find config for <{}>: id={}, sku={} ({})'.format(url, product['id'], product['sku'], sku))
            product['config'] = {}

        return product


    def get_msg(self, messages, key):
        messages = [msg for msg in messages if type(msg) == str and ('{' in msg or '}' in msg)]

        text = ''
        data = None

        for msg in messages:
            try:
                d = json.loads(msg)
            except:
                text = text + msg
            else:
                if key in d.get('d', {}).get('b', {}).get('p', ''):
                    text = msg
                    data = d
                    break

        if not data:
            try:
                data = json.loads(text)
            except Exception as e:
                pass

        return data


    def parse_specs(self, response):
        d = {}

        section = None
        for tr in response.css('table.table-specs tr'):
            if tr.css('th[colspan="2"] ::text'):
                section = ' '.join(filter(None, [t.strip() for t in tr.css('th[colspan="2"] ::text').extract()]))
                if section not in d:
                    d[section] = {}
            elif tr.css('tr>th') and tr.css('tr>td'):
                key = ' '.join(filter(None, [t.strip() for t in tr.css('tr>th ::text').extract()]))
                value = ' '.join(filter(None, [t.strip() for t in tr.css('tr>td ::text').extract()]))

                if section:
                    d[section][key] = value
                else:
                    d[key] = value

        return d
